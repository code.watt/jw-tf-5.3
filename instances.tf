resource "random_id" "random_bytes" {
  byte_length = 4
}

resource "google_compute_address" "external-ip" {
  name = "external-ip-${random_id.random_bytes.hex}"
}

resource "google_compute_instance" "instance-1" {
  name         = "instance-1-${random_id.random_bytes.hex}"
  machine_type = "f1-micro"
  hostname     = var.instance_hostname
  tags         = ["http"]
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }
  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.external-ip.address
    }
  }

  provisioner "file" {
    connection {
      type        = var.remote_exec.conn_type
      user        = var.remote_exec.username
      host        = self.network_interface[0].access_config[0].nat_ip
      private_key = file(var.remote_exec.priv_key_path)
    }

    source      = "init.sh"
    destination = "/tmp/init.sh"
  }

  provisioner "remote-exec" {
    connection {
      type        = var.remote_exec.conn_type
      user        = var.remote_exec.username
      host        = self.network_interface[0].access_config[0].nat_ip
      private_key = file(var.remote_exec.priv_key_path)
    }
    inline = [
      "chmod +x /tmp/init.sh",
      "/tmp/init.sh ${self.name} ${self.network_interface[0].network_ip}",
    ]
  }

  lifecycle {
    ignore_changes = [attached_disk]
  }
  allow_stopping_for_update = true
}

resource "cloudflare_record" "instance-1-a-record" {
  zone_id = var.cloudflare.zone_id
  name    = var.instance_hostname
  value   = google_compute_address.external-ip.address
  type    = "A"
  ttl     = 300
}

resource "google_compute_disk" "disk-1" {
  name = "disk-1-${random_id.random_bytes.hex}"
  type = "pd-standard"
  size = 10
}

resource "google_compute_disk" "disk-2" {
  name = "disk-2-${random_id.random_bytes.hex}"
  type = "pd-standard"
  size = 8
}

resource "google_compute_attached_disk" "attached-disk-1" {
  disk     = google_compute_disk.disk-1.id
  instance = google_compute_instance.instance-1.id
}

resource "google_compute_attached_disk" "attached-disk-2" {
  disk     = google_compute_disk.disk-2.id
  instance = google_compute_instance.instance-1.id
}

resource "google_compute_firewall" "allow-http" {
  name    = "allow-http"
  network = "default"

  dynamic "allow" {
    for_each = [80, 8001, 8802]
    content {
      protocol = "tcp"
      ports    = [allow.value]
    }
  }

  target_tags = ["http"]
}